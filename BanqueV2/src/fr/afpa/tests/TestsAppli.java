package fr.afpa.tests;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class TestsAppli extends TestCase {

	public static TestSuite suite() {
		TestSuite suite = new TestSuite();
		suite.addTestSuite(TestControleSaisie.class);
		suite.addTestSuite(TestControleCompte.class);
		suite.addTestSuite(TestControlePersonne.class);
		suite.addTestSuite(TestControleConseiller.class);
		suite.addTestSuite(TestControleBanque.class);
		suite.addTestSuite(TestControleAgence.class);
		
		return suite;
	}
	
	public void main(String[] args) {
		TestRunner.run(suite());
	}
}