package fr.afpa.tests;

import org.junit.Test;

import com.sun.org.apache.bcel.internal.generic.CPInstruction;

import fr.afpa.controle.ControlePersonne;
import junit.framework.TestCase;

public class TestControlePersonne extends TestCase {

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
	}
	
	@Test
	public void testIsAdminitrateur() {
		assertTrue("admin invalide", ControlePersonne.isAdminitrateur("ADM00"));
		assertFalse("admin invalide", ControlePersonne.isAdminitrateur("ADM000"));
	}
	
	@Test
	public void testIsConseiller() {
		assertTrue("conseiller invalide", ControlePersonne.isConseiller("CO0001"));
		assertFalse("conseiller invalide", ControlePersonne.isConseiller("CO001"));
	}
	
	
	@Test
	public void testIsClient() {
		assertTrue("client invalide", ControlePersonne.isClient("FF000001"));
		assertFalse("client invalide", ControlePersonne.isClient("F0000001"));
	}

	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}

}
