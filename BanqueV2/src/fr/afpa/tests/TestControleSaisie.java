package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.controle.ControleSaisie;
import junit.framework.TestCase;

public class TestControleSaisie extends TestCase {

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void testIsDate() {
		String date = "21/11/2010";
		String dateDebut = "";
		assertTrue("date incorrect", ControleSaisie.isDate(date, dateDebut));

		date = "21-11-2010";
		dateDebut = "";
		assertFalse("date incorrect", ControleSaisie.isDate(date, dateDebut));

		date = "21/11/2010";
		dateDebut = "22/11/2010";
		assertFalse("date incorrect", ControleSaisie.isDate(date, dateDebut));

		date = "21/11/2010";
		dateDebut = "20/11/2010";
		assertTrue("date incorrect", ControleSaisie.isDate(date, dateDebut));

		date = "21/11/2019";
		dateDebut = "";
		assertFalse("date incorrect", ControleSaisie.isDate(date, dateDebut));
	}

	@Test
	public void testValiderNom() {
		String nom = "Fayak";
		assertTrue("nom incorrect", ControleSaisie.validerNom(nom));
		nom = "Fayak null";
		assertTrue("nom incorrect", ControleSaisie.validerNom(nom));
		nom = "Fayak00";
		assertFalse("nom incorrect", ControleSaisie.validerNom(nom));
		nom = "";
		assertFalse("nom incorrect", ControleSaisie.validerNom(nom));
	}

	@Test
	public void testValiderMail() {
		String mail = "Fayak@gmail.ocm";
		assertTrue("mail incorrect", ControleSaisie.validerMail(mail));
		mail = "Fayak.null@gmail.ocm";
		assertTrue("mail incorrect", ControleSaisie.validerMail(mail));
		mail = "Fa..yak@gmail.ocm";
		assertFalse("mail incorrect", ControleSaisie.validerMail(mail));
		mail = "Fayak.@gmail.ocm";
		assertFalse("mail incorrect", ControleSaisie.validerMail(mail));
	}

	@Test
	public void testValiderMotDePasse() {
		String motDePasse = "Fayak";
		assertTrue("mdp incorrect", ControleSaisie.validerMotDePasse(motDePasse));
		motDePasse = "012f";
		assertTrue("mdp incorrect", ControleSaisie.validerMotDePasse(motDePasse));
		motDePasse = "fay";
		assertFalse("mdp incorrect", ControleSaisie.validerMotDePasse(motDePasse));
		motDePasse = "";
		assertFalse("mdp incorrect", ControleSaisie.validerMotDePasse(motDePasse));
	}
	
	@Test
	public void testSaisieInt() {
		String nombre = "0";
		assertTrue("nombre incorrect", ControleSaisie.saisieInt(nombre, 0, 2));
		nombre = "1";
		assertTrue("nombre incorrect",ControleSaisie.saisieInt(nombre, 0, 2));
		nombre = "2";
		assertTrue("nombre incorrect",ControleSaisie.saisieInt(nombre, 0, 2));
		nombre = "3";
		assertFalse("nombre incorrect",ControleSaisie.saisieInt(nombre, 0, 2));
		nombre = "-1";
		assertFalse("nombre incorrect",ControleSaisie.saisieInt(nombre, 0, 2));
		nombre = "";
		assertFalse("nombre incorrect", ControleSaisie.saisieInt(nombre, 0, 2));
		nombre = "Fayak";
		assertFalse("nombre incorrect",ControleSaisie.saisieInt(nombre, 0, 2));
	}

	@Test
	public void testValiderFloat() {
		String nombre = "0";
		assertTrue("nombre incorrect", ControleSaisie.saisieFloat(nombre));
		nombre = "0.12f";
		assertTrue("nombre incorrect", ControleSaisie.saisieFloat(nombre));
		nombre = "55.2";
		assertTrue("nombre incorrect", ControleSaisie.saisieFloat(nombre));
		nombre = "0.12f";
		assertTrue("nombre incorrect", ControleSaisie.saisieFloat(nombre));
		nombre = "";
		assertFalse("nombre incorrect", ControleSaisie.saisieFloat(nombre));
		nombre = "Fayak";
		assertFalse("nombre incorrect", ControleSaisie.saisieFloat(nombre));
	}
	
	@Test
	public void testValiderBoolean(){
		String bool = "tRue";
		assertTrue("boolean incorrect", ControleSaisie.saisieBoolean(bool));
		bool = "False";
		assertTrue("boolean incorrect", ControleSaisie.saisieBoolean(bool));
		bool = "truee";
		assertFalse("boolean incorrect", ControleSaisie.saisieBoolean(bool));
		bool = "Fayak";
		assertFalse("boolean incorrect", ControleSaisie.saisieBoolean(bool));
	}
	
	@Test
	public void testValiderAdresse() {
		String adr = "Fayak";
		assertTrue("boolean incorrect", ControleSaisie.validerAdresse(adr));
		adr = "a";
		assertTrue("boolean incorrect", ControleSaisie.validerAdresse(adr));
		adr = "";
		assertFalse("boolean incorrect", ControleSaisie.validerAdresse(adr));
	}
	

	@Test
	public void testisOuiOuNon() {
		String bool = "oui";
		assertTrue("boolean incorrect", ControleSaisie.isOuiNon(bool));
		bool = "non";
		assertTrue("boolean incorrect", ControleSaisie.isOuiNon(bool));
		bool = "";
		assertFalse("boolean incorrect", ControleSaisie.isOuiNon(bool));
		bool = "Fayak";
		assertFalse("boolean incorrect", ControleSaisie.isOuiNon(bool));
	}
	
	
	

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
