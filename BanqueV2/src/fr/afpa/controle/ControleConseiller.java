package fr.afpa.controle;

import fr.afpa.entite.Conseiller;

public class ControleConseiller {
	

	private ControleConseiller() {
		super();
	}

	/**
	 * Controle si le conseiller a des clients dans son portefeuille
	 * @param conseiller : le conseiller a controler
	 * @return true si le conseiller a des clients
	 */
	public static boolean hasClients(Conseiller conseiller) {
		return  !conseiller.getPortefeuilleClient().isEmpty();
	}

}
