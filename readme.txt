Projet : BanqueV2
Auteurs : Charles, Marine

D�marche � suivre : 
1) Sur le r�pertoire C cr�er l�arborescence suivante
 BanqueV2.jar se trouve dans le dossier banqueCharlesMarine

	ENV
   	|-----banqueCharlesMarine
		|-------BanqueV2.jar
		|------ressources
                  	|------ releveCompte
 		  	|------transactions
 		  	|		|-------transactionsCompte
		  	|
		  	|----sauvegarde


2) Ouvrir l�invite de commande (cmd dans la barre de recherche).
3) D�marrage de l�application, taper les commandes : 
cd C:\ENV\banqueCharlesMarineBanqueV2.jar
java -jar BanqueV2.jar


Fonctionnement de l'application : 


Lors de la cr�ation de la banque vous pouvez creer une banque vide(un admin est cr�� par defaut (login=ADM01, mdp=admin)) 
ou une banque initialis�e, la banque initialis�e contient  : 

Administrateur(login=ADM01, mdp=admin nom=admin, prenom=admin,  date de naissance=01/01/1980 , mail=admin@banque.fr) 
    Agence(codeAgence = 001, nom=agence une, adresse = 1 rue agence une)
 	|
	| -Conseiller(login=CO0002,mdp=cons1, nom=conseiller un, prenom=conseiller un,  date de naissance=01/01/1980 , mail=conseillerun@banque.fr) 
		|
		|-Client(login=0000000001,mdp=client1,identifiant=CC000001 nom=client un, prenom=client un,  date de naissance=01/01/1980 , mail=clientun@banque.fr,actif=true)
			|
			|-CompteCourant(numeroCompte=00000000001,decouvert=true,solde=100)
			|-LivretA(numeroCompte=00000000002,decouvert=false,solde=0)


le menu d'authentification s'affiche, demande le login et le mot de passe, si l'authentification est r�usssite, 
le menu de la personne authenthifi�e s'affiche, sinon redemande le login et le mot de passe.

Menu utilisateur: 
Si la personne authentifi�e est un client, il a acces � son menu : 
	- Peut consulter ses informations
	-Consulter ses comptes-Consulter les op�rationsde chaque compte
	-Faire un virement
	-Imprimer un relev� d�un compte donn� (Il faut prendre en compte les dates d�but/fin des op�rations)
	-Alimenter son compte
	-Retirer argent

Si la personne authentifi�e est un client, il a acces � son menu :  
	- Le menu client : il peut effectuer les operations du menu client pour les clients de son porte-feuille
	- Cr�er un compte (pour les clients de son porte-feuille)
	- Cr�er un client, il est ajout� � son porte-feuille
	- Changer la domiciliation d�un client (pour les clients de son porte-feuille)
	- Modifier les informations d�un client (pour les clients de son porte-feuille)

Si la personne authentifi�e est un administrateur il a acces � son menu :  
	- Le menu client et conseiller : il peut effectuer les operations du menu client et conseillers pour les clients de de la banque
	- Cr�er une agence
	- Cr�er un conseiller
	- D�sactiver un compte
	- D�sactiver un client

Lorsque l'utilisateur quitte son menu retour � la page authentification

Sauvegarde de la banque, lors de la fermeture du programme, via le menu authentification (Q)



Horaires de fonctionnement du programme : 24h/24, 7j/7



